$(document).ready(function() {

    // Cookie setting script wrapper
    var cookieScripts = function() {
        // Internal javascript called
        // Loading external javascript file
        $.cookiesDirective.loadScript({
            uri: 'external.js',
            appendTo: 'eantics'
        });
    }

    /* Call cookiesDirective, overriding any default params

      *** These are the defaults ***
        explicitConsent: true,
        position: 'top',
        duration: 10,
        limit: 0,
        message: null,        
        cookieScripts: null,
        privacyPolicyUri: 'privacy.html',
        scriptWrapper: function(){},  
        fontFamily: 'helvetica',
        fontColor: '#FFFFFF',
        fontSize: '13px',
        backgroundColor: '#000000',
        backgroundOpacity: '80',
        linkColor: '#CA0000'
        
    */

    var currentLanguage,
        siteNameIta = "Questo sito",
        siteNameEng = "This website",
    	messageIta = siteNameIta + ' non utilizza cookies per la trasmissione di informazioni di carattere personale n&eacute; cookies di profilazione di alcun tipo, ma unicamente cookies tecnici di navigazione e analytics.',
        messageEng = siteNameEng + ' does not use cookies to transmit information of a personal data, it does not use profiling cookies or systems for tracking users. This website uses only browsing and analytics cookies.',
        linkIta = '<nobr>Maggiori informazioni >></nobr>',
        linkEng = '<nobr>Further information >></nobr>',
        dontShowIta = 'Non mostrare nuovamente questo messaggio',
        dontShowEng = "Don't show this message again",
        urlIta = '/it/informativa-cookies',
        urlEngl = '/en/cookies-disclaimer',
        isEngl;

    var isEngl = _spPageContextInfo.currentCultureName != "it-IT" ? true : false;

    var siteName = isEngl ? siteNameEng : siteNameIta;
    var theMessage = isEngl ? messageEng : messageIta;
    var theLink = isEngl ? linkEng : linkIta;
    var dontShow = isEngl ? dontShowEng : dontShowIta;
    var disclaimerUrl = isEngl ? urlEngl : urlIta;

    $.cookiesDirective({
        siteName: siteName,
        explicitConsent: false,
        position: 'bottom',
        duration: 999,
        limit: 0,
        cookieScripts: null,
        privacyPolicyUri: disclaimerUrl,
        scriptWrapper: cookieScripts,
        fontFamily: 'inherit',
        fontColor: '#FFFFFF',
        fontSize: '13px',
        backgroundColor: '#1A1A1A',
        backgroundOpacity: '1.0',
        linkColor: '#ffffff',
        message: theMessage,
        impliedDisclosureText: ' ',
        impliedSubmitText: dontShow,
        privacyPolicyLinkText: theLink,
        inlineAction: false
        /*,
      impliedSubmitText: 'OK',
      explicitCookieAcceptButtonText: 'OK',
      explicitCookieAcceptanceLabel: 'I understand and accept',
      multipleCookieScriptBeginningLabel: ' This site uses ',
      explicitCookieDeletionWarning: ' You may delete the cookies. '*/
    });



});